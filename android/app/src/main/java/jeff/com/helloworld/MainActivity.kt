package jeff.com.helloworld

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.json.JSONException
import org.json.JSONObject
import org.liquidplayer.service.MicroService
import org.liquidplayer.service.MicroService.EventListener
import java.net.URI
import java.net.URISyntaxException


class MainActivity : AppCompatActivity() {
    // IMPORTANT: Replace this with YOUR server's address or name
    private val serverAddr = "10.0.2.2:8088"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        // Our 'ready' listener will wait for a ready event from the micro service.  Once
        // the micro service is ready, we'll ping it by emitting a "ping" event to the
        // service.
        val readyListener = EventListener { service, event, payload -> service.emit("ping") }

        // Our micro service will respond to us with a "pong" event.  Embedded in that
        // event is our message.  We'll update the textView with the message from the
        // micro service.
        val pongListener = EventListener { service, event, payload ->
            // NOTE: This event is typically called inside of the micro service's thread, not
            // the main UI thread.  To update the UI, run this on the main thread.
            Handler(Looper.getMainLooper()).post {
                try {
                    textView.text = payload.getString("message")
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

        // Our start listener will set up our event listeners once the micro service Node.js
        // environment is set up
        val startListener = MicroService.ServiceStartListener { service, synchronizer ->
            service.addEventListener("ready", readyListener)
            service.addEventListener("pong", pongListener)
        }

        // When our button is clicked, we will launch a new instance of our micro service.
        button.setOnClickListener {
            try {
                val uri = URI("http://$serverAddr/service.js")
                val service = MicroService(this@MainActivity, uri, startListener)
                service.start()
            } catch (e: URISyntaxException) {
                e.printStackTrace()
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
